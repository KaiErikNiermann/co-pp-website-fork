# Glossary

* **ASCII** a standard for encoding characters in memory. ASCII characters have values in the range 0 to 255.

* **assembly** the act of translating human-readable assembly into machine code to be executed by the computer or virtual machine.

* **binary** a (possibly) non-human readable file format consisting of bytes. Usually the individual bytes have to be combined into integers to make sense of the contents.

* **branch** a branch is an instruction that can divert the control flow of the program, deviating from executing the program instructions in order.

* **byte** a sequence of bits, signifying the smallest unit of addressable memory. On modern computer architectures a byte consists of 8 bits.

* **endianness** the order in which the bytes are arranged in a numerical datatype. big-endian has the most significant bit first, while little-endian has the least-significant bit first.

* **hexadecimal** a way of notating a number. Rather than using base 10 (as in decimal), hexadecimal uses base 16. Usually used for byte notation, as two hex digits can represent one byte.

* **local frame** a piece of memory allocated for a certain subroutine (function). The local frame contains information about local variables as well as the stack used in the subroutine.

* **pointer** a memory object (e.g., 64 bit number) which refers to a certain memory location. Accessing the pointed-to object using the pointer is called dereferencing a pointer.

* **stack** a linear last-in-first-out (LIFO) datatype with the following operations: `top`, `pop`, `push` ,`size`.

* **stack machine** a machine architecture that operates on a stack, rather than on registers.

* **word** a data type represented as a four-byte big-endian integer.
